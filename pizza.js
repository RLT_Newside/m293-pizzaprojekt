//Hier sind die JSON Daten
let jsonData = [
    {
        "name": "Piccante",
        "prize": "16$",
        "id": 1,
        "ingredients": [
            "Tomato",
            "Mozzarella",
            "Spicy Salami",
            "Chilies",
            "Oregano"
        ],
        "imageUrl": "pizza/piccante.jpg"
    },
    {
        "name": "Giardino",
        "prize": "14$",
        "id": 2,
        "ingredients": [
            "Tomato",
            "Mozzarella",
            "Artichokes",
            "Fresh Mushrooms"
        ],
        "imageUrl": "pizza/giardino.jpg"
    },
    {
        "name": "Prosciuotto e funghi",
        "prize": "15$",
        "id": 3,
        "ingredients": [
            "Tomato",
            "Mozzarella",
            "Ham",
            "Fresh Mushrooms",
            "Oregano"
        ],
        "imageUrl": "pizza/prosciuttoefunghi.jpg"
    },
    {
        "name": "Quattro formaggi",
        "prize": "13$",
        "id": 4,
        "ingredients": [
            "Tomato",
            "Mozzarella",
            "Parmesan",
            "Gorgonzola"
        ],
        "imageUrl": "pizza/quattroformaggi.jpg"
    },
    {
        "name": "Quattro stagioni",
        "prize": "17$",
        "id": 5,
        "ingredients": [
            "Tomato",
            "Mozzarella",
            "Ham",
            "Artichokes",
            "Fresh Mushrooms"
        ],
        "imageUrl": "pizza/quattrostagioni.jpg"
    },
    {
        "name": "Stromboli",
        "prize": "12$",
        "id": 6,
        "ingredients": [
            "Tomato",
            "Mozzarella",
            "Fresh Chilies",
            "Olives",
            "Oregano"
        ],
        "imageUrl": "pizza/stromboli.jpg"
    },
    {
        "name": "Verde",
        "prize": "13$",
        "id": 7,
        "ingredients": [
            "Tomato",
            "Mozzarella",
            "Broccoli",
            "Spinach",
            "Oregano"
        ],
        "imageUrl": "pizza/verde.jpg"
    },
    {
        "name": "Rustica",
        "prize": "15$",
        "id": 8,
        "ingredients": [
            "Tomato",
            "Mozzarella",
            "Ham",
            "Bacon",
            "Onions",
            "Garlic",
            "Oregano"
        ],
        "imageUrl": "pizza/rustica.jpg"
    }
]
//Hier werden die JSON Daten aufgerufen
jsonData.forEach(pizza => {
    var pizzaVar = document.querySelector("#pizzaContainer");
    var pizzaDiv = document.createElement("div");
    pizzaDiv.innerHTML = '<div class="container"><div class="item"><div><img src="' + pizza.imageUrl + '" class="pizzaimg"></div><div class="line"><h4>' + pizza.name +'</h4><h4>' + pizza.prize + '</h4><button onclick="incrementValue()"><img class="cart" src="pizza/shoppingcart.png" alt="addtocart"></button></div><div class="ingredients"><p>' + pizza.ingredients + '</p></div></div></div>'
    pizzaVar.appendChild(pizzaDiv);
});
let totalCounter = 0; //Counter wird auf 0 gesetzt

function incrementValue() { //Jedes Mal wenn die Funktion aufgerufen wird, zählt der Counter eins drauf
    totalCounter++;
    document.getElementById('totalCounter').textContent = totalCounter;
}