//Hier sind die JSON Daten
let jsonData = [
    {
        "name": "Coke",
        "prize": "2$",
        "id": 1,
        "imageUrl": "soft/coke.jpg",
        "volume":[
            "50cl",
            "33cl",
            "1l"
        ]
    },
    {
        "name": "Fanta",
        "prize": "2$",
        "id": 2,
        "imageUrl": "soft/fanta.jpg",
        "volume":[
            "50cl",
            "33cl",
            "1l"
        ]
    },
    {
        "name": "Pepsi",
        "prize": "2$",
        "id": 3,
        "imageUrl": "soft/pepsi.jpg",
        "volume":[
            "50cl",
            "33cl",
            "1l"
        ]
    },
    {
        "name": "Red bull",
        "prize": "3$",
        "id": 4,
        "imageUrl": "soft/redbull.jpg",
        "volume": "50cl"
    }
]
// Hier werden die JSON Daten aufgerufen
jsonData.forEach(soft => {
    var softDrinks = document.querySelector("#softContainer");
    var softDrinksDiv = document.createElement("div");
    softDrinksDiv.innerHTML = '<div class="softitem"><img class="softimg "src="' + soft.imageUrl + '"> <p>' + soft.name + '</p> <div class="line"><select class = "selectClass"><option> ' + soft.volume[0] + '</option><option>' + soft.volume[1] + '</select><p>' + soft.prize + '</p> <button onclick="incrementValue()"><img class="cart" src="pizza/shoppingcart.png" alt="addtocart"></button> </div></div>';
    softDrinks.appendChild(softDrinksDiv);
});
let totalCounter = 0; //Counter wird auf 0 gesetzt

function incrementValue() { //Jedes Mal wenn die Funktion aufgerufen wird, zählt der Counter eins drauf
    totalCounter++;
    document.getElementById('totalCounter').textContent = totalCounter;
}