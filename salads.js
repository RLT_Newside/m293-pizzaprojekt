//Hier sind die JSON Daten
const jsonData =
[
    {
        "name": "Green salad with tomatoe",
        "prize": "4$",
        "id": 1,
        "ingredients": [
            "Iceberg lettuce",
            "Tomatoes"
        ],
        "imageUrl": "https://farm6.staticflickr.com/5087/5358599242_7251dc7de4.jpg"
    },
    {
        "name": "Tomato salad with mozzarella",
        "prize": "5$",
        "id": 2,
        "ingredients": [
            "Tomato",
            "Mozzarella"
        ],
        "imageUrl": "salat/GreenSaladWithTomato.jpg"
    },
    {
        "name": "Field salad with egg",
        "prize": "4$",
        "id": 3,
        "ingredients": [
            "Field salad",
            "Egg"
        ],
        "imageUrl": "https://farm9.staticflickr.com/8223/8372222471_662acd24f6.jpg"
    },
    {
        "name": "Rocket with parmesan",
        "prize": "5$",
        "id": 4,
        "ingredients": [
            "Rocket",
            "Parmesan"
        ],
        "imageUrl": "https://farm8.staticflickr.com/7017/6818343859_bb69394ff2.jpg"
    }
]
jsonData.forEach(salad => {
    var saladVar = document.querySelector("#saladcontainer");
    var saladDiv = document.createElement("div");
    saladDiv.innerHTML = '<div class="container"><div class="item"><img src="' + salad.imageUrl +'" alt="Green salad with tomatoe" class="saladimg"><h4>' + salad.name + '</h4><p>' + salad.ingredients + '</p><div class="line"><select name="dressings"><option >Italian dressing</option><option >French dressing</option></select>4$<button onclick="incrementValue()"><img class="cart" src="pizza/shoppingcart.png" alt="addtocart"></button></div></div></div> '
    saladVar.appendChild(saladDiv);

});
let totalCounter = 0; //Counter wird auf 0 gesetzt

function incrementValue() { //Jedes Mal wenn die Funktion aufgerufen wird, zählt der Counter eins drauf
    totalCounter++;
    document.getElementById('totalCounter').textContent = totalCounter;
}